"""
Web client specific functionality.

Module to be used by `sbws <https://gitlab.torproject.org/tpo/network-health/sbws>`_,
https://gitlab.torproject.org/juga/bwscanner_webserver and
https://gitlab.torproject.org/juga/relay_bw.

"""
