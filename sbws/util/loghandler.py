"""Log handlers."""

import gzip
import logging
import os
import shutil


class GzipTimedRotatingFileHandler(logging.handlers.TimedRotatingFileHandler):
    """Compress all dayly log files, except the current day one."""

    def doRollover(self):  # noqa: N802
        super().doRollover()
        log_dir = os.path.dirname(self.baseFilename)
        to_compress = [
            os.path.join(log_dir, f)
            for f in os.listdir(log_dir)
            if f.startswith(
                os.path.basename(os.path.splitext(self.baseFilename)[0])
            )
            and not f.endswith((".gz", ".log"))
        ]
        for f in to_compress:
            if os.path.exists(f):
                with open(f, "rb") as _old, gzip.open(f + ".gz", "wb") as _new:
                    shutil.copyfileobj(_old, _new)
                os.remove(f)
